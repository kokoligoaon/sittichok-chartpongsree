import React, { Component } from 'react';
import './App.css';
import UrlShortener from './components/url-shortener/url-shortener';

class App extends Component {
  render() {
    return (
      <div className="App">
        <UrlShortener />
      </div>
    );
  }
}

export default App;
