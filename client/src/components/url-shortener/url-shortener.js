import React, { Component } from 'react';
import './url-shortener.css';
const axios = require('axios');

class UrlShortener extends Component {
  constructor(props){
    super(props);
    this.state = {
      shortenUrl: '',
      fullUrl: '',
      url: '',
      customUrl: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event){
    const name = event.target.name;
    const value = event.target.value;
    this.setState({
      [name]: value
    });
    console.log(`on change:${name}  ${value}`);
  }

  handleSubmit(event){
    console.log("on submit");
    event.preventDefault();
    const req = {
      url: this.state.url,
      customUrl: this.state.customUrl
    }
    axios.post('/api/url-shortener', req)
      .then(res => {
        this.setState({
          shortenUrl: res.data.shortenUrl,
          fullUrl: res.data.fullUrl
        });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        <h2>Enter a long URL to make shorten URL:</h2>
        <form onSubmit={this.handleSubmit} className="request">
          <input name='url' type='text' value={this.state.url} onChange={this.handleChange}></input>
          <button type='submit'>Make shorten URL</button>
          <h3>Custom alias (optional):</h3>
          https://shrt.com/
          <input name='customUrl' type='text' value={this.state.customUrl} onChange={this.handleChange}></input>
        </form>

        <div className="result">
          Shorten URL: {this.state.shortenUrl}<br></br>
          URL: {this.state.fullUrl}
        </div>
      </div>

    );
  }
}

export default UrlShortener;
