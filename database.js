var mysql = require('mysql')
var pool = mysql.createPool({
    connectionLimit: 10,
    host: 'localhost',
    user: 'root',
    password: 'rootadmin123',
    database: 'url_shortener'
})

module.exports = pool