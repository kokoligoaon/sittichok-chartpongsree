const express = require('express');

const app = express();

const Joi = require('joi');

const pool = require('./database');

app.use(express.json());

app.use(ignoreFavicon);

function ignoreFavicon(req, res, next) {
    if (req.originalUrl === '/favicon.ico') {
      res.status(204).json({nope: true});
    } else {
      next();
    }
}

app.post('/api/url-shortener', (req, res) => {
    // validate
    const schema = {
        url: Joi.string().required().regex(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/),
        customUrl: Joi.string().optional().allow('')
	}
	const result = Joi.validate(req.body, schema);
	if(result.error){
		res.status(400).send(result.error.details[0].message);
		return;
    }
    
    // result
    const urlShortener = {
        shortenUrl: '', 
        fullUrl: ''
    };

    if(req.body.customUrl){
        // do create custom shorten url
        const url_id = keyToId(req.body.customUrl);

        // validate
        const url_key = idToKey(url_id);
        if(url_key !== req.body.customUrl){
            res.status(400).send("Bad Request");
            return;
        }

        const url = req.body.url;

        pool.getConnection((err, connection) => { 
            if (err) throw err;

            connection.query(`INSERT INTO url (url_id, url_key, url, created) VALUES (${url_id}, '${url_key}', '${url}', now())`, (error, results, fields) => {
                if (error){
                    connection.release();
                    throw error;
                } 
    
                urlShortener.shortenUrl = shortenHost+url_key;
                urlShortener.fullUrl = url;
                res.json(urlShortener);
    
                connection.release();
            });
        });
    }else{
        // do create shorten url
        pool.getConnection((err, connection) => { 
            if (err) throw err;

            // 1. generate id
            connection.query('INSERT INTO sequence_table (created) VALUE (now())', (error, results, fields) => {
                if (error){
                    connection.release();
                    throw error;
                } 
                
                if(results.insertId){
                    const url_id = results.insertId;
                    const url_key = idToKey(url_id);
                    const url = req.body.url;

                    // 2. save shorten url
                    connection.query(`INSERT INTO url (url_id, url_key, url, created) VALUES (${url_id}, '${url_key}', '${url}', now())`, (error, results, fields) => {
                        if (error){
                            connection.release();
                            throw error;
                        } 

                        urlShortener.shortenUrl = shortenHost+url_key;
                        urlShortener.fullUrl = url;
                        res.json(urlShortener);

                        connection.release();
                    });
                }
            });
        });
    }
});

// Map to store 62 possible characters 
const map = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789".split(""); 
const shortenHost = 'https://shrt.com/';

function idToKey(n) { 
    var key = ''; 
    // Convert given integer id to a base 62 number 
    do { 
        console.log("n%62:"+n%62);
        key += (map[n%62]);
        n = parseInt(n/62); 
    } while (n)
  
    // Reverse key
    return key.split("").reverse().join("");
}

function keyToId(key) { 
    var id = 0; 
    var a = 'a'.charCodeAt(0);
    var z = 'z'.charCodeAt(0);
    var A = 'A'.charCodeAt(0);
    var Z = 'Z'.charCodeAt(0);
    var n0 = '0'.charCodeAt(0);
    var n9 = '9'.charCodeAt(0);

    for (var i=0; i < key.length; i++) 
    { 
        var charCode = key.charCodeAt(i);
        if (a <= charCode && charCode <= z) 
          id = id*62 + charCode - a; 
        if (A <= charCode && charCode <= Z) 
          id = id*62 + charCode - A + 26; 
        if (n0 <= charCode && charCode <= n9) 
          id = id*62 + charCode - n0 + 52; 
    } 
    return id; 
} 

app.get('/:key', (req, res) => {
    console.log('key:'+req.params.key);
    const id = keyToId(req.params.key);
    var url = '';
    console.log('id:'+id);
    pool.getConnection((err, connection) => { 
        if (err) throw err;

        console.log("REQUEST");
        connection.query(`SELECT url FROM url WHERE url_id = ${id}`, (error, results, fields) => {
            console.log("RESULT");
            if (error){
                connection.release();
                throw error;
            } 

            if(results.length>0){
                url = results[0].url;
                console.log("url:"+url);
                if(!url.startsWith("http")){
                    url = "http://"+url;
                }
                res.redirect(301, url);
            }else{
                res.status(404).send('The URL was not found.');
            }

            connection.release();
        });
    });
});

const port = 5000;

app.listen(port, () => console.log(`Server started on port ${port}`));